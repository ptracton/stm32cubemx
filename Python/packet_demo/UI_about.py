#! /usr/bin/env python3

from PyQt4 import QtCore
from PyQt4 import QtGui


class UI_about(QtGui.QDialog):

    '''
    About GUI class
    '''

    def __init__(self, parent=None):
        super(UI_about, self).__init__(parent)
        self.about_info = QtGui.QLabel(
            "STM32 Packet Demo Application")
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.about_info)
        self.setLayout(vbox)
        self.setWindowTitle("STM32 Packet Demo")
        pass
