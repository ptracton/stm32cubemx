#! /usr/bin/env python3
'''
Learning Python Lab 6 central widget
'''

import json
import sys
import traceback
from PyQt4 import QtCore
from PyQt4 import QtGui

import STM32Lib


class UI_central(QtGui.QDialog):
    '''
    Our top level UI widget
    '''

    def __init__(self, parent=None):
        super(UI_central, self).__init__(parent)
        hbox = QtGui.QHBoxLayout()
        vbox = QtGui.QVBoxLayout()

        json_file = open('packet_demo.json')
        json_str = json_file.read()
        self.json_data = json.loads(json_str)

        #
        # Serial Port UI Component
        #
        self.SerialPort = STM32Lib.SerialPortUI.SerialPortUI()
        hbox.addLayout(self.SerialPort.getLayout())

        self.ConnectButton = QtGui.QPushButton("Connect")
        self.ConnectButton.setEnabled(False)
        hbox.addWidget(self.ConnectButton)

        self.PingButton = QtGui.QPushButton("Ping")
        self.PingButton.setEnabled(False)
        hbox.addWidget(self.PingButton)
        vbox.addLayout(hbox)
        
        self.cpu_id = STM32Lib.CPU_ID_UI.CPU_ID_UI()
        vbox.addLayout(self.cpu_id.getLayout())

        self.tabs = STM32Lib.BoardUI.BoardUI()
        vbox.addLayout(self.tabs.getLayout())
       
        #
        # Display the layout
        #
        self.setLayout(vbox)

        self.L3GD20_displayed = False
        self.LSM303DLHC_displayed = False
        
        #
        # Connect UI elements to actions
        #
        self.connect(self.SerialPort.SerialOpenButton,
                     QtCore.SIGNAL("clicked()"),
                     self.SerialPortOpen)

        self.connect(self.SerialPort.SerialCloseButton,
                     QtCore.SIGNAL("clicked()"),
                     self.SerialPortClose)

        self.connect(self.ConnectButton,
                     QtCore.SIGNAL("clicked()"),
                     self.ConnectButtonClicked)

        self.connect(self.PingButton,
                     QtCore.SIGNAL("clicked()"),
                     self.PingButtonClicked)
        self.protocol = STM32Lib.Protocol.Protocol(
            port=self.SerialPort.serial_port)

        self.memory_tab = STM32Lib.Memory.Memory(protocol = self.protocol)
        
        self.tabs.addTab(self.memory_tab, "Memory")
        
        return
    
    def SerialPortOpen(self):
        try:
            self.SerialPort.serial_port.setPort(
                self.SerialPort.SerialPortComboBox.currentText())
            self.SerialPort.serial_port.open()
            print("Serial Port Open")
        except:
            print("Serial Port Open Error")
            print("Exception in user code:")
            print("-"*60)
            traceback.print_exc(file=sys.stdout)
            print("-"*60)
            return
        self.ConnectButton.setEnabled(True)
        self.PingButton.setEnabled(True)
        return

    def SerialPortClose(self):
        self.SerialPort.serial_port.close()
        self.ConnectButton.setEnabled(False)
        self.cpu_id.setValues()
        self.cpu_id.setEnabled(False)
        print("Serial Port Closed")
        return
    
    def ConnectButtonClicked(self):
        print("Connect Button Clicked")
        data = self.protocol.Connect()
        #print("CPUID 0x%x" % data[0])
        #print("Memory Size 0x%x" % data[1])
        #print("Rev ID 0x%x" % data[2])
        #print("Dev ID 0x%x" % data[3])
        #print("Hal ID 0x%x" % data[4])
        self.cpu_id.setValues(hex(data[0]), hex(data[1]),
                              hex(data[2]), hex(data[3]), hex(data[4]))

        for x in self.json_data['boards']:
            if self.json_data['boards'][x]['cpu']['device_id'] == hex(data[3]):
                self.cpu_id.board_name_value.setText(
                    self.json_data['boards'][x]['name'])
                self.memory_tab.cpu_id = x
                self.memory_tab.json_data = self.json_data
                print("CPU ID %s" % self.memory_tab.cpu_id)
                for tab in self.json_data['boards'][x]['tabs']:
                    print (tab)
                    
                    if ((tab == "L3GD20") and (self.L3GD20_displayed is False)):
                        self.L3GD20_displayed = True
                        self.L3GD20 = STM32Lib.L3GD20.L3GD20(protocol=self.protocol)
                        self.tabs.addTab(self.L3GD20, "L3GD20")

                    if ((tab == "LSM303DLHC") and (self.LSM303DLHC_displayed is False)):
                        self.LSM303DLHC_displayed = True
                        self.LSM303DLHC = STM32Lib.LSM303DLHC.LSM303DLHC(
                            protocol=self.protocol)
                        self.tabs.addTab(self.LSM303DLHC, "LSM303DLHC")

        self.cpu_id.setEnabled(True)
        return
    
    def PingButtonClicked(self):
        print("Ping Button Clicked")
        self.protocol.Ping()
        return
