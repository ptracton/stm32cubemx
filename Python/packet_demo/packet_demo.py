#! /usr/bin/env python3

import sys

from PyQt4 import QtGui

try:
    sys.path.append('/home/ptracton/src/software/STM32CubeMX/Python')
#    sys.path.append('C:\\Users\\ptracton\\src\\software\\STM32CubeMX\\Python\\STM32Lib')
    import STM32Lib
except:
    print("Failed to import STM32Lib")
    sys.exit(-1)
    
import UI

if __name__ == "__main__":
    print("Starting Packet Demo")
    app = QtGui.QApplication(sys.argv)
    gui = UI.UI()
    gui.show()
    app.exec_()
