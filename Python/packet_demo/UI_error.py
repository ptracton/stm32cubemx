#! /usr/bin/env python3

from PyQt4 import QtCore
from PyQt4 import QtGui


class UI_error(QtGui.QDialogButtonBox):

    '''
    About GUI class
    '''

    def __init__(self, parent=None, message="Error"):
        super(UI_error, self).__init__(parent)
        vbox = QtGui.QVBoxLayout()
        messageLabel = QtGui.QLabel(message)
        vbox.addWidget(messageLabel)
        self.addButton(QtGui.QDialogButtonBox.Ok)
        self.setLayout(vbox)
        self.setWindowTitle("STM32 Packet Error")
        pass
