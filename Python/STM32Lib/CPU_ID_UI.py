#! /usr/bin/env python3

#
from PyQt4 import QtCore
from PyQt4 import QtGui


class CPU_ID_UI:
    """
    UI for CPU and software revision information
    """
    
    def __init__(self):
        self.vbox = QtGui.QVBoxLayout()
        self.hbox2 = QtGui.QHBoxLayout()
        self.hbox = QtGui.QHBoxLayout()

        self.cpuid_label = QtGui.QLabel("CPU ID:")
        self.cpuid_value = QtGui.QLabel("None")

        self.memory_size_label = QtGui.QLabel("Memory Size:")
        self.memory_size_value = QtGui.QLabel("None")

        self.revision_id_label = QtGui.QLabel("Revision ID:")
        self.revision_id_value = QtGui.QLabel("None")

        self.device_id_label = QtGui.QLabel("Device ID:")
        self.device_id_value = QtGui.QLabel("None")

        self.hal_id_label = QtGui.QLabel("HAL ID:")
        self.hal_id_value = QtGui.QLabel("None")

        self.board_name_label = QtGui.QLabel("Board:")
        self.board_name_value = QtGui.QLabel("None")
        
        self.hbox.addWidget(self.cpuid_label)
        self.hbox.addWidget(self.cpuid_value)
        self.hbox.addWidget(self.memory_size_label)
        self.hbox.addWidget(self.memory_size_value)
        self.hbox.addWidget(self.revision_id_label)
        self.hbox.addWidget(self.revision_id_value)
        self.hbox.addWidget(self.device_id_label)
        self.hbox.addWidget(self.device_id_value)
        self.hbox.addWidget(self.hal_id_label)
        self.hbox.addWidget(self.hal_id_value)

        self.hbox2.addWidget(self.board_name_label)
        self.hbox2.addWidget(self.board_name_value)
        
        self.vbox.addLayout(self.hbox2)
        self.vbox.addLayout(self.hbox)
        self.setEnabled(False)
        return

    def getLayout(self):
        return self.vbox

    def setEnabled(self, enable=False):
        self.cpuid_label.setEnabled(enable)
        self.cpuid_value.setEnabled(enable)
        self.memory_size_label.setEnabled(enable)
        self.memory_size_value.setEnabled(enable)
        self.revision_id_label.setEnabled(enable)
        self.revision_id_value.setEnabled(enable)
        self.device_id_label.setEnabled(enable)
        self.device_id_value.setEnabled(enable)
        self.hal_id_label.setEnabled(enable)
        self.hal_id_value.setEnabled(enable)
        self.board_name_label.setEnabled(enable)
        self.board_name_value.setEnabled(enable)
        return

    def setValues(self, cpuid=None, memory_size=None, rev_id=None,
                  dev_id=None, hal_id=None):

        self.cpuid_value.setText(str(cpuid))
        self.memory_size_value.setText(str(memory_size))
        self.device_id_value.setText(str(dev_id))
        self.revision_id_value.setText(str(rev_id))
        self.hal_id_value.setText(str(hal_id))
                                
        return
