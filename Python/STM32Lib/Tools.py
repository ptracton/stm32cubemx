#! /usr/bin/env python3


def long_to_bytes(data=None):
    return [int(data >> i & 0xff) for i in (0,8,16,24)]

def short_to_bytes(data=None):
    return [int(data >> i & 0xff) for i in (0,8)]

def bytes_to_long(data = None):
    long_val = data[0]
    long_val |= data[1] << 8
    long_val |= data[2] << 16
    long_val |= data[3] << 24
    return  long_val

def bytes_to_short(data = None):
    long_val = data[0]
    long_val |= data[1] << 8
    return long_val
