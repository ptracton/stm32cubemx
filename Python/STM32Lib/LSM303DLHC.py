#! /usr/bin/env python3

from PyQt4 import QtCore
from PyQt4 import QtGui


class LSM303DLHC(QtGui.QWidget):
    """
    """
    
    def __init__(self, parent=None, protocol=None):
        super(LSM303DLHC, self).__init__(parent)

        self.protocol = protocol
        
        self.hbox = QtGui.QHBoxLayout()
        self.temperature_label = QtGui.QLabel("Temperature")
        self.temperature_value = QtGui.QLabel("")
        self.temperature_button = QtGui.QPushButton("Get Temperature")

        self.hbox.addWidget(self.temperature_label)
        self.hbox.addWidget(self.temperature_value)
        self.hbox.addWidget(self.temperature_button)

        self.setLayout(self.hbox)

        self.connect(self.temperature_button,
                     QtCore.SIGNAL("clicked()"),
                     self.TemperatureButtonClicked)
        
        return
    
    def TemperatureButtonClicked(self):
        print("Getting Temperature ")
        temperature = self.protocol.GetTemperatureLSM303DLHC()
        print(temperature)
        self.temperature_value.setText(str(temperature[0]))
        return
