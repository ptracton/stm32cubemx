#! /usr/bin/env python3

import array
import enum
import threading
import zlib

import Tools


@enum.unique
class PacketType(enum.Enum):
    PING = 0x00,
    GET_CPU_ID = 0x01,
    GET_MEMORY = 0x02,
    SET_MEMORY = 0x03,
    GET_TEMPERATURE_L3GD20 = 0x04,
    GET_TEMPERATURE_LSM303DLHC = 0x05,
    ALWAYS_LAST = 0x05

    
class Packet:
    def __init__(self, packet_type=None, data=[], port=None):

        #
        # Reading thread variables
        # 
        self.receiver_thread = None
        self.status = 0
        self.packet_type = packet_type.value[0]
        self.size = len(data)
        self.data = data
        self.crc = 0xFFFFFFFF
        self.binary_data = []
        self.crc_match = False
        self._receiving_packet = False
        #
        # The Serial Port to use
        #
        self.Port = port
        
        return

    def _toArray(self):
        self.binary_data = [0xC7]
        self.binary_data.append(self.packet_type)
        self.binary_data.extend(Tools.short_to_bytes(self.size))
        if (self._receiving_packet):
            print ("RECEIVING ARRAY")
            self.binary_data.extend(Tools.short_to_bytes(self.status))
        self.binary_data.extend(self.data)
        return
    
    def CalculateCRC(self):        
        self._toArray()
        crc = array.array('B', self.binary_data).tostring()
        self.crc = zlib.crc32(crc[1:])
        if (not self._receiving_packet):
            print(self.binary_data)        
            print(Tools.long_to_bytes(self.crc))
            
        self.binary_data.extend(Tools.long_to_bytes(self.crc))
        return

    def __str__(self):
        string = "Packet Type = 0x%x\n" % self.packet_type
        string += "Packet Size = 0x%x\n" % self.size
        string += "Packet Data = %s\n" % str(self.data[0:4])
        string += "Packet CRC  = 0x%x\n" % self.crc
        return string
    
    def _start_reader(self):
        """
        Start reader thread
        """
        self.receiver_thread = threading.Thread(target=self.reader)
        self.receiver_thread.setDaemon(True)
        self.receiver_thread.start()
        return

        
    def reader(self):
        """
        This is the reader thread, it will read in 1 packet and terminate the thread
        """

        self.size = 0
        self.packet_type = 0
        self.data = []
        self.crc = 0xFFFFFFFF
        
        ##
        ## Each of these lines, gets a byte rom the Serial Port in little endian order
        ## and converts it into an integer based on the bytes it has received.  It is 
        ## a little much since we are only reading a single byte at a time.
        ##
        self.packet_type = int.from_bytes(self.Port.read(1), byteorder='little')
        print ("Packet Type:  0x%x"% self.packet_type)
        
        self.size = int.from_bytes(self.Port.read(2), byteorder='little')
        print ("Packet Size: 0x%x" % self.size)

        self.status = int.from_bytes(self.Port.read(2), byteorder='little')
        print ("Packet Status 0x%x"% self.status)
        
        if (self.size > 0 ):
            for x in range(self.size):
                data = int.from_bytes(self.Port.read(1), byteorder='little')
                self.data.append(data)
                print("Data[%d]: 0x%x" %(x, data))
                del(data)
        
        crc = int.from_bytes(self.Port.read(4), byteorder='little')
        print ("CRC: 0x%x"% crc)

        self.CalculateCRC()
        if (crc != self.crc):
            self.crc_match = False
            print ("CRC MISMATCH 0x%x != 0x%x\n" %(crc,self.crc))
        else:
            self.crc_match = True
            print ("Valid Response")
        return

    def ReceivePacket(self):
        """
        This method will receive a packet sent from the MCU.  It will then return 
        the result from checking the CRC.  This allows callers to know if this is a
        good packet or not.
        """
        self.size = 0
        self.packet_type = 0
        self.status = 0
        self.data = []
        self.crc = 0xFFFFFFFF
        self._receiving_packet = True
        
        if (self.Port == None):
            print ("Receive Packet: No Port!")
            return
        
        print ("\nRECEIVE:")
        self._start_reader()
        while (self.receiver_thread.is_alive()):
            pass

        return 
    
    def TransmitPacket(self):
        """
        This method will send the packet to the MCU
        """
        print ("\nTRANSMIT:")
        
        if (self.Port == None):
            print ("Transmit Packet: No Port!")
            return
            
        #
        # Create a list and append all the bytes for the packet to it
        # 
        packet = [0xC7]
        packet.append(self.packet_type)
        packet.extend(Tools.short_to_bytes(self.size))

        if (self.size > 0):
            packet.extend(self.data)

        #
        # Get the CRC for this packet and add them to the end of it
        #
        self._receiving_packet = False
        self.CalculateCRC()
        packet.extend(Tools.long_to_bytes(self.crc))
        #print(packet)

        transmit = array.array('B', packet).tobytes()
        
        #
        # Send the bytes in a binary format via the Serial Port
        #
        #print ("\n\nTRANSMIT: ", transmit)
        self.Port.transmit_binary(transmit)
        return
