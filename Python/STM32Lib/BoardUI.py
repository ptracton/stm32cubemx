#! /usr/bin/env python3

'''
UI for Board Specific Features
'''

from PyQt4 import QtCore
from PyQt4 import QtGui

class BoardUI:
    """
    """

    def __init__(self):

        self.hbox = QtGui.QHBoxLayout()
        self.tabs = QtGui.QTabWidget()

#        self.tabs.addTab(self.basic_features_tab, "Basic Features")
        
        self.hbox.addWidget(self.tabs)

        return

    def addTab(self, tab=None, title=None):
        self.tabs.addTab(tab, title)
        return
    
    def getLayout(self):
        return self.hbox
    
    def setEnabled(self, enable=False):
        return
