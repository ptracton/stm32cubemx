#! /usr/bin/env python3

import Packet
import Tools


class Protocol:
    """
    """
    
    def __init__(self, port=None):
        self.port = port
        return

    def Ping(self):
        """
        """
        packet = Packet.Packet(packet_type=Packet.PacketType.PING,
                               port=self.port)
        packet.CalculateCRC()
        packet.TransmitPacket()
        packet.ReceivePacket()

        return

    def Connect(self):
        """
        """
        packet = Packet.Packet(packet_type=Packet.PacketType.GET_CPU_ID,
                               port=self.port)
        packet.CalculateCRC()
        packet.TransmitPacket()
        packet.ReceivePacket()

        if packet.crc_match:
            cpuid = Tools.bytes_to_long(packet.data[0:4])
            memory_size = Tools.bytes_to_short(packet.data[4:6])
            rev_id = Tools.bytes_to_short(packet.data[6:8])
            dev_id = Tools.bytes_to_short(packet.data[8:10])
            hal_id = Tools.bytes_to_long(packet.data[10:14])
            results_list = [cpuid, memory_size, rev_id, dev_id, hal_id]
        else:
            results_list = [0, 0, 0, 0, 0]
        return results_list

    def GetTemperatureL3GD20(self):
        packet = Packet.Packet(packet_type=Packet.PacketType.GET_TEMPERATURE_L3GD20,
                               port=self.port)
        packet.CalculateCRC()
        packet.TransmitPacket()
        packet.ReceivePacket()
        if packet.crc_match:
            temperature = packet.data[0:7][0]
            results_list = [temperature]
        else:
            results_list = ["Error"]
        return results_list
    
    def GetTemperatureLSM303DLHC(self):
        packet = Packet.Packet(packet_type=Packet.PacketType.GET_TEMPERATURE_LSM303DLHC,
                               port=self.port)
        packet.CalculateCRC()
        packet.TransmitPacket()
        packet.ReceivePacket()
        if packet.crc_match:
            temperature = packet.data[0:7][0]
            results_list = [temperature]
        else:
            results_list = ["Error"]
        return results_list

    def GetMemory(self, address=None, length=None):
        print("Get Memory Protocol %s %s" % (address, length))
        if (address is None) or (length is None):
            return []
        
        packet = Packet.Packet(packet_type=Packet.PacketType.GET_MEMORY, port=self.port)
        packet.data.append(address)
        packet.data.append(length)
        packet.CalculateCRC()
        packet.TransmitPacket()
        packet.ReceivePacket()
        if packet.crc_match:
            print("Get Memory CRC Valid")
            print(packet.data)
            return_list = packet.data
            
        return return_list
    
