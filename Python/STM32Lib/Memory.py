#! /usr/bin/env python3

from PyQt4 import QtCore
from PyQt4 import QtGui


class Memory(QtGui.QWidget):
    """
    """

    def __init__(self, parent=None, protocol=None, json_data=None,
                 cpu_id=None):
        super(Memory, self).__init__(parent)
        self.protocol = protocol
        self.json_data = json_data
        self.cpu_id = cpu_id
        
        self.vbox = QtGui.QVBoxLayout()

        self.hbox_buttons = QtGui.QHBoxLayout()
        self.get_memory_button = QtGui.QPushButton("Get Memory")
        self.set_memory_button = QtGui.QPushButton("Set Memory")

        address_hbox = QtGui.QHBoxLayout()
        address_label = QtGui.QLabel("Address:")
        self.address_entry = QtGui.QLineEdit()
        self.address_entry.setMaxLength(10)
        address_hbox.addWidget(address_label)
        address_hbox.addWidget(self.address_entry)

        length_hbox = QtGui.QHBoxLayout()
        length_label = QtGui.QLabel("Length (Words)")
        self.length_entry = QtGui.QLineEdit()
        self.length_entry.setMaxLength(6)
        length_hbox.addWidget(length_label)
        length_hbox.addWidget(self.length_entry)

        self.hbox_buttons.addWidget(self.get_memory_button)
        self.hbox_buttons.addWidget(self.set_memory_button)

        self.connect(self.get_memory_button,
                     QtCore.SIGNAL("clicked()"),
                     self.GetMemoryClicked)

        self.connect(self.set_memory_button,
                     QtCore.SIGNAL("clicked()"),
                     self.SetMemoryClicked)

        self.vbox.addLayout(address_hbox)
        self.vbox.addLayout(length_hbox)
        self.vbox.addLayout(self.hbox_buttons)
        self.setLayout(self.vbox)
        return


    def _check_address(self, address, length, start, size):
        if (address < start) or (address+length > start+size):
            return False
        
        return True
    
    
    def GetMemoryClicked(self):
        print("Get Memory Clicked")
        sram_start = int(self.json_data['boards'][self.cpu_id]['cpu']['sram_start'], 16)
        sram_size = int(self.json_data['boards'][self.cpu_id]['cpu']['sram_size'], 16)
        flash_start = int(self.json_data['boards'][self.cpu_id]['cpu']['flash_start'], 16)
        flash_size = int(self.json_data['boards'][self.cpu_id]['cpu']['flash_size'], 16)
        
        #print(sram_start)
        #print(sram_size)
        #print(flash_start)
        #print(flash_size)

        try:
            address = int(self.address_entry.displayText(), 16)
            length = int(self.length_entry.displayText(), 16)
        except ValueError:
            print("Address Error!")
            return
        
        #print(address)
        #print(length)

        if (self._check_address(address, length, sram_start, sram_size) or
            self._check_address(address, length, flash_start, flash_size)):
            get_memory = self.protocol.GetMemory(address, length)
        else:
            get_memory = None
        
        return get_memory
    
    def SetMemoryClicked(self):
        print("Set Memory Clicked")
        return
