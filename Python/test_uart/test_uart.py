#! /usr/bin/env python3


import array
import binascii
import sys 
import serial
import serial.tools.list_ports


def sendData(port, data):
    print("Trans Binary: ", data)
    #print (bytearray(data))
    transmit = array.array('B', data).tobytes()
    print("Transmit", transmit)
    crc32 = (binascii.crc32(transmit[1:]) & 0xFFFFFFFF)
    #print(type(crc32))
    print("CRC32 = 0x%x \n" % crc32)
    transmit_crc32 = crc32.to_bytes(4, byteorder='little')
    #print(transmit_crc32)
    port.write(transmit)
    port.write(transmit_crc32)

    #port.write(data)
    return

if __name__ == "__main__":
    com_port_list = list(serial.tools.list_ports.comports())
    ports = [x[0] for x in com_port_list]
    #print(ports)

    SerialPort = serial.Serial()
    #SerialPort.setPort('COM4')
    SerialPort.setPort('/dev/ttyUSB0')
    SerialPort.setBaudrate(115200)
    SerialPort.setByteSize(serial.EIGHTBITS)
    SerialPort.setParity(serial.PARITY_NONE)
    SerialPort.setStopbits(serial.STOPBITS_ONE)

    SerialPort.open()
    send = [0xC7, 0x00, 0x04, 0x00, 0x11, 0xAA, 0x93, 0x94]
#            0xde, 0xad, 0xbe, 0xef]
    #print(send)
    sendData(SerialPort, send)
