
/*******************************************************************************

 INCLUDES

******************************************************************************/

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

#include "FreeRTOS.h"
#include "task.h"
#include "stm32f3_discovery.h"

#include "crc32.h"
#include "packets.h"
#include "RX_Task.h"

/*******************************************************************************

 LOCAL VARIABLES

******************************************************************************/


///
///
///
static RX_Task_States_Enum rx_state;

///
///
///
static Packet_TypeDef rx_pkt;

///
///
///
static Packet_TypeDef rx_pkt_copy;

///
///
///
static PacketHandler packet_handler_table[PACKET_ALWAYS_LAST];

///
///
///
static osEvent event;

extern osMessageQId RX_QueueHandle;
extern osMessageQId TX_QueueHandle;
extern UART_HandleTypeDef huart2;

#define RX_BUFFER_SIZE 64
static uint8_t rx_buffer[RX_BUFFER_SIZE];
static uint32_t rx_bytes_to_receive;
static uint32_t rx_index;
#define MESSAGE_NO_TIMEOUT 0x00
extern DMA_HandleTypeDef hdma_usart2_rx;
/*******************************************************************************

 PRIVATE  API

******************************************************************************/
void handler_ping(Packet_TypeDef * pkt){

  BSP_LED_On(LED_RED);
  
  return;
}

void rx_add_handler(uint32_t index, PacketHandler handler){

  if (index > PACKET_ALWAYS_LAST){
    return;
  }

  packet_handler_table[index] = handler;
  
  return;
}

void rx_task_init(void){
  uint32_t i;
  
  for (i=0; i < RX_BUFFER_SIZE; i++){
    rx_buffer[i] = 0;
  }
	
  Packet_Clear(&rx_pkt);
  Packet_Clear(&rx_pkt_copy);

  rx_add_handler(PACKET_PING, handler_ping);

  rx_state = RX_STATE_IDLE;
  rx_bytes_to_receive = 4;


  BSP_LED_Init(LED3 | LED4 | LED5 | LED6 | LED7 | LED8 | LED9 | LED10);
  BSP_LED_Init(LED_BLUE);
  BSP_LED_Off(LED_RED);
  //	HAL_UART_Receive_DMA(&huart2, &rx_buffer[0], rx_bytes_to_receive);	
  return;
}

/*******************************************************************************

 PUBLIC API

******************************************************************************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  osStatus status;  
  
  status = osMessagePut(RX_QueueHandle, 0, MESSAGE_NO_TIMEOUT);
  if (osOK != status){
    BSP_LED_On(LED_RED);	
    while(1);
  }
  return;
}



void RX_Task(void){
  rx_task_init();
  
  while(1){		
    //osStatus status;
    HAL_StatusTypeDef hal_status;
    
    hal_status = HAL_UART_Receive_DMA(&huart2, &rx_buffer[0], rx_bytes_to_receive);	
    huart2.Instance->ICR = (UART_CLEAR_EOBF | UART_CLEAR_OREF);		
    if (HAL_OK  != hal_status){
      BSP_LED_On(LED_GREEN);	
      while(1);		
    }					
    
    event = osMessageGet(RX_QueueHandle, osWaitForever);		
    if (osEventMessage == event.status){			
      
      
      switch (rx_state){
      case RX_STATE_IDLE:{
        if (PACKET_PREAMBLE_BYTE==rx_buffer[0] ){
          rx_pkt.type = (PacketType_Enum) rx_buffer[1];
          rx_pkt.size = rx_buffer[2] <<8 | rx_buffer[3];
          rx_state = RX_STATE_GET_DATA;
          rx_bytes_to_receive = rx_pkt.size;
        }
        break;
      }
      case RX_STATE_GET_DATA:{
        uint32_t i;
        for (i=0; i< rx_pkt.size; i++){
          rx_pkt.data[i] = rx_buffer[i];
        }					
        rx_state = RX_STATE_GET_CRC;
        rx_bytes_to_receive = 4;
        break;
      }
        
      case RX_STATE_GET_CRC:{						
        rx_pkt.crc = rx_buffer[0] | (rx_buffer[1] << 8) | (rx_buffer[2] << 16) | (rx_buffer[3] << 24);
        rx_state = RX_STATE_IDLE;
        rx_bytes_to_receive = 4;
        break;
      }
        
      default:{
        rx_state = RX_STATE_IDLE;
        break;
      }	
      }			
    }

  };
  
}
