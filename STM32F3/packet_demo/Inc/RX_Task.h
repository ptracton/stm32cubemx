#ifndef __RX_TASK_H__
#define __RX_TASK_H__

typedef enum{
  RX_STATE_IDLE = 0,
  RX_STATE_GET_TYPE,
  RX_STATE_GET_SIZE,
  RX_STATE_GET_DATA,
  RX_STATE_GET_CRC,
  RX_STATE_ERROR,
  RX_STATE_LAST
}RX_Task_States_Enum;


#endif
