#include "stm32f3xx_hal.h"

static uint8_t echo;
extern UART_HandleTypeDef huart2;

void uart_echo_entry(void){
  echo = 0;
  HAL_UART_Receive_IT(&huart2, &echo,1);
  HAL_UART_Transmit_IT(&huart2, &echo,1);  
  __enable_irq();
  return;
}


 void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  HAL_UART_Receive_IT(huart, &echo,1);
  HAL_UART_Transmit_IT(huart, &echo,1);
  return;
}
