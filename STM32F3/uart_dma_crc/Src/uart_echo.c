#include "stm32f3xx_hal.h"
#include <crc32.h>

//static uint32_t echo;
//static uint8_t temp_char;
extern UART_HandleTypeDef huart2;
extern CRC_HandleTypeDef hcrc;
volatile uint32_t crc_value;
static uint8_t rx_buffer[32];

void uart_echo_entry(void){

  // echo = 0;
  crc_value = 0;
  HAL_UART_Receive_DMA(&huart2, &rx_buffer[0], 8);
  __enable_irq();
  return;
}


 void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  volatile uint32_t transmit_crc=0;
  
  
  HAL_UART_Receive_DMA(&huart2, &rx_buffer[0], 8);
  //  HAL_UART_Transmit_DMA(&huart2, &rx_buffer[0], 1); 

  //
  // Grab the last 4 bytes of the message as a CRC value
  //
  transmit_crc = rx_buffer[4] | rx_buffer[5] << 8 | rx_buffer[6] << 16 | rx_buffer[7] << 24;

  //
  // Check
  //
  crc_value = crc32(&rx_buffer[0], 4);

  //
  // Hang on error!
  //
  if (transmit_crc != crc_value){
    while(1);
  }
  
  return;
}
