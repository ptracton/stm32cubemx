
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "l3gd20.h"
#include "lsm303dlhc.h"
#include "sensors_ic.h"

static uint8_t l3gd20_present;
static uint8_t lsm303dlhc_present;

void sensors_ic_init(void){
  LSM303DLHCMag_InitTypeDef mag_init;
  
  BSP_LED_Init(LED3 | LED4 | LED5 | LED6 | LED7 | LED8 | LED9 | LED10);

  l3gd20_present = 0;
  L3GD20_Init(L3GD20_MODE_ACTIVE );
  if ( I_AM_L3GD20  == L3GD20_ReadID()){
    l3gd20_present = 1;
  }else{
    BSP_LED_Toggle(LED_BLUE);
  }

  lsm303dlhc_present = 0;
  COMPASSACCELERO_IO_Init();
  mag_init.Temperature_Sensor = LSM303DLHC_TEMPSENSOR_ENABLE;
  LSM303DLHC_MagInit(&mag_init);
  if (I_AM_LMS303DLHC == LSM303DLHC_AccReadID() ){
    lsm303dlhc_present = 1;
  }else{
    BSP_LED_Toggle(LED_RED_2);
  }
  
  return;
}
