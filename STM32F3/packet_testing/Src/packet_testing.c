
/*******************************************************************************

 INCLUDES

******************************************************************************/
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "packets.h"
#include "crc32.h"
#include "l3gd20.h"
#include "lsm303dlhc.h"
#include "packet_testing.h"

/*******************************************************************************

 EXTERNAL VARIABLES

******************************************************************************/
extern UART_HandleTypeDef huart2;
uint8_t DataRX;
uint8_t rx_byte;
uint8_t DataTX;
uint16_t status = 0;
/*******************************************************************************

 LOCAL VARIABLES

******************************************************************************/
///
///
///
static struct RXPacket_StructDef rx_pkt;
static struct RXPacket_StructDef rx_pkt_copy;
static struct TXPacket_StructDef tx_pkt;

///
///
///
static uint32_t rx_index;

///
///
///
PACKET_STATES_TypeDef state;

///
///
///
static PacketHandler packet_handler_table[PACKET_ALWAYS_LAST];

//
//
//
static uint8_t l3gd20_present;
static uint8_t lsm303dlhc_present;

/*******************************************************************************

 PRIVATE  API

******************************************************************************/

void respond_ping(void){
  uint8_t * ptr;
    
  tx_pkt.type = PACKET_PING;
  tx_pkt.size = 0;
  tx_pkt.status = status;
  ptr = (uint8_t *) &tx_pkt.type;
  tx_pkt.crc = crc32(ptr, tx_pkt.size +5);
  DataTX = 1;
  return;
}

void handler_get_cpu_id(struct RXPacket_StructDef * pkt){
  uint8_t * ptr;
  uint32_t * memory_size_ptr;
  uint32_t cpuid = SCB->CPUID;
  uint32_t rev_id = HAL_GetREVID();
  uint32_t dev_id = HAL_GetDEVID();
  uint32_t hal_id = HAL_GetHalVersion();
	
  memory_size_ptr = (uint32_t *)0x1FFFF7CC;
  tx_pkt.status = 0xDEAD;  
  tx_pkt.type = PACKET_GET_CPU_ID;
  tx_pkt.size = 14;
  
  tx_pkt.data[0] = cpuid;	
  tx_pkt.data[1] = cpuid >>8;	
  tx_pkt.data[2] = cpuid >> 16;
  tx_pkt.data[3] = cpuid >> 24;
		
  tx_pkt.data[4] = *memory_size_ptr;	
  tx_pkt.data[5] = *memory_size_ptr>>8;	
	
  tx_pkt.data[6] = rev_id;	
  tx_pkt.data[7] = rev_id>>8;	
	
  tx_pkt.data[8] = dev_id;	
  tx_pkt.data[9] = dev_id>>8;	
	
  tx_pkt.data[10] = hal_id;	
  tx_pkt.data[11] = hal_id>>8;	
  tx_pkt.data[12] = hal_id>>16;
  tx_pkt.data[13] = hal_id>>24;
	
  ptr = (uint8_t *) &tx_pkt.type;
  tx_pkt.crc = crc32(ptr, tx_pkt.size +5);
  DataTX = 1;	
  return;
}

void handler_ping(struct RXPacket_StructDef * pkt){

  BSP_LED_Toggle(LED_RED);
  respond_ping();
  return;
}

void handler_get_temperature_l3gd20(struct RXPacket_StructDef * pkt){
  uint8_t temperature;
  uint8_t * ptr;
  
  TXPacket_Clear(&tx_pkt);

  if (! l3gd20_present ){
    respond_ping();
  }else{
    GYRO_IO_Read(&temperature, L3GD20_OUT_TEMP_ADDR, 1);
    tx_pkt.status = 0xDEAD;  
    tx_pkt.type = PACKET_GET_TEMPERATURE_L3GD20;
    tx_pkt.size = 1;
    tx_pkt.data[0] = temperature;
    ptr = (uint8_t *) &tx_pkt.type;
    tx_pkt.crc = crc32(ptr, tx_pkt.size +5);
    DataTX = 1;    
  }
  
  return;
}

void handler_get_temperature_lsm303dlhc(struct RXPacket_StructDef * pkt){
  int16_t temperature;
  uint8_t * ptr;
  
  TXPacket_Clear(&tx_pkt);

  if (! lsm303dlhc_present ){
    respond_ping();
  }else{
    temperature =  LSM303DLHC_MagGetTemperature();
    tx_pkt.status = 0xDEAD;  
    tx_pkt.type = PACKET_GET_TEMPERATURE_LSM303DLHC;
    tx_pkt.size = 2;
    tx_pkt.data[0] = temperature;
    tx_pkt.data[1] = temperature >> 8;
    ptr = (uint8_t *) &tx_pkt.type;
    tx_pkt.crc = crc32(ptr, tx_pkt.size +5);
    DataTX = 1;    
  }
  
  return;
}

void rx_add_handler(uint32_t index, PacketHandler handler){

  if (index > PACKET_ALWAYS_LAST){
    return;
  }

  packet_handler_table[index] = handler;
  
  return;
}

/*******************************************************************************

 PUBLIC API

******************************************************************************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

  HAL_UART_Receive_IT(&huart2, &rx_byte, 1);
  DataRX = 1;

  return;
}

void Packet_Testing_Init(void){
  uint32_t i;
  LSM303DLHCMag_InitTypeDef mag_init;
  
  for (i=0; i<PACKET_ALWAYS_LAST; i++){	
    packet_handler_table[i] = NULL;
  }
	
  RXPacket_Clear(&rx_pkt);
  RXPacket_Clear(&rx_pkt_copy);
  TXPacket_Clear(&tx_pkt);
  
  rx_add_handler(PACKET_PING, handler_ping);
  rx_add_handler(PACKET_GET_CPU_ID, handler_get_cpu_id);
  rx_add_handler(PACKET_GET_TEMPERATURE_L3GD20, handler_get_temperature_l3gd20);
  rx_add_handler(PACKET_GET_TEMPERATURE_LSM303DLHC, handler_get_temperature_lsm303dlhc);
	
  rx_index = 0;
  state = STATE_IDLE;
  DataRX = 0;
  DataTX = 0;
  
  BSP_LED_Init(LED3 | LED4 | LED5 | LED6 | LED7 | LED8 | LED9 | LED10);

  l3gd20_present = 0;
  L3GD20_Init(L3GD20_MODE_ACTIVE );
  if ( I_AM_L3GD20  == L3GD20_ReadID()){
    l3gd20_present = 1;
  }else{
    BSP_LED_Toggle(LED_BLUE);
  }


  COMPASSACCELERO_IO_Init();
  lsm303dlhc_present = 0;
  mag_init.Temperature_Sensor = LSM303DLHC_TEMPSENSOR_ENABLE;
  LSM303DLHC_MagInit(&mag_init);
  if (I_AM_LMS303DLHC == LSM303DLHC_AccReadID() ){
    lsm303dlhc_present = 1;
  }else{
    BSP_LED_Toggle(LED_RED_2);
  }
  
  __enable_irq();
  return;
}

void Packet_Testing_Task(uint8_t byte){
  uint8_t * ptr;

	
  switch(state){
    
  case STATE_IDLE: {

    if (PACKET_PREAMBLE_BYTE == byte){
      rx_index = 0;
      state = STATE_CAPTURE_PACKET_TYPE;
    }
    break;
  }

  case STATE_CAPTURE_PACKET_TYPE:{
    rx_pkt.type = (PacketType_Enum) byte;
    state = STATE_CAPTURE_PACKET_SIZE_LO;
    break;
  }

  case STATE_CAPTURE_PACKET_SIZE_LO:{
    rx_pkt.size |= byte;
    state = STATE_CAPTURE_PACKET_SIZE_HI;

    break;
  }


  case STATE_CAPTURE_PACKET_SIZE_HI:{
    rx_pkt.size |= byte << 8;
    if (rx_pkt.size >0){
      state = STATE_CAPTURE_PACKET_DATA;
    }else{
      state = STATE_CAPTURE_PACKET_CRC3;
    }
    rx_index = 0;
    break;
  }
           
  case STATE_CAPTURE_PACKET_DATA:{
    rx_pkt.data[rx_index] = byte;
    rx_index ++;
    if (rx_index >= rx_pkt.size){
      state = STATE_CAPTURE_PACKET_CRC3;
    }else{
      state = STATE_CAPTURE_PACKET_DATA;
    }
    break;
  }
    
  case STATE_CAPTURE_PACKET_CRC3:{
    rx_pkt.crc = byte ;
    state = STATE_CAPTURE_PACKET_CRC2;
    break;
  }

  case STATE_CAPTURE_PACKET_CRC2:{
    rx_pkt.crc |= byte << 8;
    state = STATE_CAPTURE_PACKET_CRC1;

    break;
  }

  case STATE_CAPTURE_PACKET_CRC1:{
    rx_pkt.crc |= byte << 16;
    state = STATE_CAPTURE_PACKET_CRC0;

    break;
  }

  case STATE_CAPTURE_PACKET_CRC0:{
    uint32_t crc_val;
    
    rx_pkt.crc |= byte << 24;
    state = STATE_IDLE;
		

    memcpy(&rx_pkt_copy, &rx_pkt, sizeof(struct RXPacket_StructDef));
    ptr = (uint8_t *) &rx_pkt_copy.type;
    crc_val = crc32(ptr, rx_pkt_copy.size +3);

    if (crc_val == rx_pkt_copy.crc){      
      if (NULL != packet_handler_table[rx_pkt_copy.type] && 
          (rx_pkt_copy.type < PACKET_ALWAYS_LAST)){
        packet_handler_table[rx_pkt_copy.type](&rx_pkt_copy);
      } 

    }
    
    break;
  }

   
  default:{
    rx_index = 0;
    state = STATE_IDLE;
  }
    
  }
    
 
  return;
}


void Packet_Transmit(void){
  //uint8_t * ptr = (uint8_t *) &tx_pkt;

  
  //HAL_UART_Transmit(&huart2, ptr, 5, 0xFFFF);
  //  HAL_UART_Transmit(&huart2, (uint8_t *) status, 2, 0xFFFF);
  //  //  if (tx_pkt.size){
  //    HAL_UART_Transmit(&huart2, &tx_pkt.data[0], tx_pkt.size, 0xFFFF);
  //  }
  HAL_UART_Transmit(&huart2, (uint8_t *) &tx_pkt, tx_pkt.size+5, 0xFFFF);
  
  HAL_UART_Transmit(&huart2, (uint8_t *) &tx_pkt.crc, 4, 0xFFFF);
  return;
}
