
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

#include "FreeRTOS.h"
#include "task.h"

#include "leds_driver.h"

extern UART_HandleTypeDef huart2;
static uint8_t rx_byte;
extern osMessageQId uart_rx_queueHandle;
extern osMessageQId uart_tx_queueHandle;
static osEvent event;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  osStatus status;
  HAL_UART_Receive_IT(huart, &rx_byte,1);
  status = osMessagePut(uart_rx_queueHandle, rx_byte, 0);

  if (osOK != status){
    // ????
  }
  return;
}

void uart_rx_init(void){
  
  rx_byte = 0;
  HAL_UART_Receive_IT(&huart2, &rx_byte,1);
  LEDS_Init();
  return;
}

LEDS_COLOR_TypeDef number_to_led(uint8_t number){
  LEDS_COLOR_TypeDef led;

  switch(number){
  case 0x30: led =  LED_RED_0; break;
  case 0x31: led =  LED_ORANGE_0; break;
  case 0x32: led =  LED_GREEN_0; break;
  case 0x33: led =  LED_BLUE_0; break;
  case 0x34: led =  LED_RED_1; break;
  case 0x35: led =  LED_ORANGE_1; break;
  case 0x36: led =  LED_GREEN_1; break;
  case 0x37: led =  LED_BLUE_1; break;
  }
  return led;
}

void UART_RX_Task(void){
  
  uart_rx_init();
  
  while(1){
    osStatus status;
    
    event = osMessageGet(uart_rx_queueHandle, osWaitForever);
    if (osEventMessage == event.status){

      //
      // Toggle LED based on input character
      //
      if ((event.value.v >= 0x30) && (event.value.v < 0x38)){
        LEDS_Toggle(number_to_led(event.value.v));
      }
      
      //
      // Ok receive, return the character
      //
      status = osMessagePut(uart_tx_queueHandle, event.value.v, 0);
      if (osOK != status){
        // ????
      }
    }
    
  }
  
}
