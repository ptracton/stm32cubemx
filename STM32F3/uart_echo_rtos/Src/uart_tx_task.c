#include "stm32f3xx_hal.h"
#include "cmsis_os.h"


#include "FreeRTOS.h"
#include "task.h"

extern UART_HandleTypeDef huart2;
extern osMessageQId uart_tx_queueHandle;
static osEvent event;

void uart_tx_init(void){
  return;
}

void UART_TX_Task(void){
  
  uart_tx_init();
  
  while(1){
    HAL_StatusTypeDef status;
    event = osMessageGet(uart_tx_queueHandle, osWaitForever);
    if (osEventMessage == event.status){
      status = HAL_UART_Transmit(&huart2, (uint8_t *) &event.value.v, 1, osWaitForever);
      if (HAL_OK != status){
        //????
      }
    }
  }
  
}
