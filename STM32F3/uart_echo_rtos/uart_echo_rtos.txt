Configuration	uart_echo_rtos
STM32CubeMX 	4.13.0
Date	03/01/2016
MCU	STM32F303VCTx



PERIPHERALS	MODES	FUNCTIONS	PINS
SYS	TIM1	SYS_VS_tim1	VP_SYS_VS_tim1
USART2	Asynchronous	USART2_RX	PA3
USART2	Asynchronous	USART2_TX	PA2



Pin Nb	PINs	FUNCTIONs	LABELs
8	PC14-OSC32_IN*	RCC_OSC32_IN	OSC32_IN
9	PC15-OSC32_OUT*	RCC_OSC32_OUT	OSC32_OUT
12	PF0-OSC_IN*	RCC_OSC_IN	OSC_IN
13	PF1-OSC_OUT*	RCC_OSC_OUT	OSC_OUT
23	PA0	GPIO_Input	B1 [Blue PushButton]
25	PA2	USART2_TX	
26	PA3	USART2_RX	
39	PE8	GPIO_Output	LD4 [Blue Led]
40	PE9	GPIO_Output	LD3 [Red Led]
41	PE10	GPIO_Output	LD5 [Orange Led]
42	PE11	GPIO_Output	LD7 [Green Led]
43	PE12	GPIO_Output	LD9 [Blue Led]
44	PE13	GPIO_Output	LD10 [Red Led]
45	PE14	GPIO_Output	LD8 [Orange Led]
46	PE15	GPIO_Output	LD6 [Green Led]
70	PA11*	USB_DM	DM
71	PA12*	USB_DP	DP
72	PA13*	SYS_JTMS-SWDIO	SWDIO
76	PA14*	SYS_JTCK-SWCLK	SWCLK
89	PB3*	SYS_JTDO-TRACESWO	SWO



SOFTWARE PROJECT

Project Settings : 
Project Name : uart_echo_rtos
Project Folder : C:\Users\tractp1\src\software\STM32CubeMX\STM32F3\uart_echo_rtos
Toolchain / IDE : MDK-ARM V5
Firmware Package Name and Version : STM32Cube FW_F3 V1.4.0


Code Generation Settings : 
STM32Cube Firmware Library Package : Copy all used libraries into the project folder
Generate peripheral initialization as a pair of '.c/.h' files per IP : No
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : Balanced Size/Speed






