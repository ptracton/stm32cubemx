#ifndef __PACKETS_H__
#define __PACKETS_H__

#include <stdint.h>

#define MAX_PACKET_COUNT 32

#define PACKET_PREAMBLE_BYTE (0xC7)

#define PACKET_SIZE_BITS   10
#define PACKET_SIZE_MASK   0x000003FF
#define PACKET_DATA_DEPTH  64
/*******************************************************************************
Data Structures
 ******************************************************************************/

///
/// This enum lists all of the different packets that this application
/// can respond to.
///
typedef enum{
  PACKET_PING = 0,
  PACKET_GET_CPU_ID,
  PACKET_GET_MEMORY,
  PACKET_SET_MEMORY,
  PACKET_GET_TEMPERATURE_L3GD20,
  PACKET_GET_TEMPERATURE_LSM303DLHC,
  PACKET_ALWAYS_LAST
} PacketType_Enum;

///
/// This is the basic packet data structure
///

#if defined ( __GNUC__ )
struct  __attribute__ ((__packed__)) RXPacket_StructDef{
  PacketType_Enum type;
  uint16_t size;
  uint8_t data[PACKET_DATA_DEPTH];
  uint32_t crc;
} ;

struct  __attribute__ ((__packed__)) TXPacket_StructDef{
  PacketType_Enum type;
  uint16_t size;
  uint16_t status;
  uint8_t data[PACKET_DATA_DEPTH];
  uint32_t crc;
} ;

#else
struct RXPacket_StructDef{
  __packed PacketType_Enum type;
  __packed uint16_t size;
  __packed uint8_t data[PACKET_DATA_DEPTH];
  __packed uint32_t crc;
} ;

struct TXPacket_StructDef{
  __packed PacketType_Enum type;
  __packed uint16_t size;
  __packed uint16_t status;
  __packed uint8_t data[PACKET_DATA_DEPTH];
  __packed uint32_t crc;
} ;
#endif

///
/// This is a function pointer to a packet handler.  The RX
/// task will need to create a table of these.
///
typedef void (*PacketHandler)(struct RXPacket_StructDef *);

///
/// This enum is used as a return value from the CRC
/// check.  This lets the caller know if there was a valid match
///

typedef enum{
  PACKET_CRC_INVALID = 0x00,
  PACKET_CRC_VALID
} Packet_CRC_Enum;
/*******************************************************************************

PUBLIC API

 ******************************************************************************/
void RXPacket_Clear(struct RXPacket_StructDef * pkt);
void RXPacket_Copy(struct RXPacket_StructDef * src, struct RXPacket_StructDef *dst);
Packet_CRC_Enum RXPacket_CheckCRC(struct RXPacket_StructDef *pkt);

void TXPacket_Clear(struct TXPacket_StructDef * pkt);
void TXPacket_Copy(struct TXPacket_StructDef * src, struct TXPacket_StructDef *dst);
Packet_CRC_Enum TXPacket_CheckCRC(struct TXPacket_StructDef *pkt);


#endif
