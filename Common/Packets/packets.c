
#include <stdint.h>
#include "crc32.h"
#include "packets.h"


///
/// \name Packet_Clear
/// \brief Clears all fields in the packet
/// \input Packet_Typedef * pkt 
/// \return void
///

void RXPacket_Clear(struct RXPacket_StructDef * pkt){
  uint32_t i;
  pkt->type = PACKET_PING;
  pkt->size = 0;
  pkt->crc = 0;
  for (i=0; i< PACKET_DATA_DEPTH; i++){
    pkt->data[i] =0;
  }
  
  return;
}

void TXPacket_Clear(struct TXPacket_StructDef * pkt){
  uint32_t i;
  pkt->type = PACKET_PING;
  pkt->size = 0;
  pkt->status= 0;
  pkt->crc = 0;
  for (i=0; i< PACKET_DATA_DEPTH; i++){
    pkt->data[i] =0;
  }
  
  return;
}


///
/// \name Packet_Copy
/// \rbrief Copies the source packet to the destination packet
/// \input struct Packet_StructDef * src
/// \input struct Packet_StructDef * dst
/// \return void
/// 
void RXPacket_Copy(struct RXPacket_StructDef * src, struct RXPacket_StructDef * dst){
  uint32_t i;
  dst->type = src->type;
  dst->size = src->size;
  dst->crc = src->crc;
  for (i=0; i<= dst->size; i++){
    dst->data[i] = src->data[i];
  }
  return;
}

void TXPacket_Copy(struct TXPacket_StructDef * src, struct TXPacket_StructDef * dst){
  uint32_t i;
  dst->type = src->type;
  dst->size = src->size;
  dst->status = src->status;
  dst->crc = src->crc;
  for (i=0; i<= dst->size; i++){
    dst->data[i] = src->data[i];
  }
  return;
}

///
/// \name Packet_CheckCRC
/// \brief Calculate the CRC of the packet and compare with value from packet structure
/// \input struct Packet_StructDef * pkt
/// \return Packet_CRC_Enum
///
Packet_CRC_Enum RXPacket_CheckCRC(struct RXPacket_StructDef * pkt){
  Packet_CRC_Enum ret_val = PACKET_CRC_VALID;

  if (crc32(pkt, pkt->size+3) != pkt->crc){
    ret_val = PACKET_CRC_INVALID;
  }
  return ret_val;
}

Packet_CRC_Enum TXPacket_CheckCRC(struct TXPacket_StructDef * pkt){
  Packet_CRC_Enum ret_val = PACKET_CRC_VALID;

  if (crc32(pkt, pkt->size+5) != pkt->crc){
    ret_val = PACKET_CRC_INVALID;
  }
  return ret_val;
}
