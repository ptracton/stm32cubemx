
#include <stdint.h>
#include "leds.h"

void LEDS_On(uint16_t led){
  HAL_GPIO_WritePin(LEDS_GPIO, led, GPIO_PIN_SET);
  return;
}

void LEDS_Off(uint16_t led){
  HAL_GPIO_WritePin(LEDS_GPIO, led, GPIO_PIN_RESET);
  return;
}

void LEDS_Toggle(uint16_t led){
  HAL_GPIO_TogglePin(LEDS_GPIO, led);
  return;
}


void LEDS_Init(void){
  uint16_t i;
  
  for (i=0; i < NUMBER_OF_LEDS; i++  ){
    LEDS_Off(GPIO_PIN_8 <<i);
  }
  
  return;
}

