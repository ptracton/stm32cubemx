#ifndef __LEDS_DRIVER_H__
#define __LEDS_DRIVER_H__

#ifdef STM32F303xC
#include "stm32f3xx_hal.h"
#define LEDS_GPIO GPIOE
#define NUMBER_OF_LEDS 8
typedef enum{
  LED_NORTH = GPIO_PIN_9,
  LED_EAST  = GPIO_PIN_11,
  LED_SOUTH = GPIO_PIN_13,
  LED_WEST  = GPIO_PIN_15,

  LED_NORTH_EAST = GPIO_PIN_10,
  LED_SOUTH_EAST = GPIO_PIN_12,
  LED_NORTH_WEST = GPIO_PIN_8,
  LED_SOUTH_WEST = GPIO_PIN_14,
  
} LEDS_DIRECTION_TypeDef;

typedef enum{
  LED_RED_0    = GPIO_PIN_9,
  LED_GREEN_0  = GPIO_PIN_11,
  LED_RED_1    = GPIO_PIN_13,
  LED_GREEN_1  = GPIO_PIN_15,

  LED_ORANGE_0 = GPIO_PIN_10,
  LED_BLUE_0   = GPIO_PIN_12,
  LED_BLUE_1   = GPIO_PIN_8,
  LED_ORANGE_1 = GPIO_PIN_14,
  
} LEDS_COLOR_TypeDef;

#endif

void LEDS_On(uint16_t led);
void LEDS_Off(uint16_t led);
void LEDS_Toggle(uint16_t led);
void LEDS_Init(void);

#endif
